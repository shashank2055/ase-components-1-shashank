﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graphical_Programming_application
{
    public partial class Form1 : Form
    {
        Circle circle;
        Rectangle rectangle;
        Square square;
        Color c;
        Color fill;
        int moveX, moveY;
        int thickness = 3;
        Boolean drawCircle;
        Boolean drawRectangle;
        Boolean drawTriangle;
        Boolean drawSquare;
        List<Circle> circleObjects;
        List<Rectangle> rectangleObjects;
        List<Triangle> triangleObjects;
        List<Square> squareObjects;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            circleObjects = new List<Circle>();
            rectangleObjects = new List<Rectangle>();
            triangleObjects = new List<Triangle>();
            squareObjects = new List<Square>();
           
            fill = Color.Transparent;
            c = Color.DarkGreen;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            if (drawCircle == true)
            {
                foreach (Circle circleObject in circleObjects)
                {
                    circleObject.draw(g, c, thickness);
                }
            }
            if (drawRectangle == true)
            {
                foreach (Rectangle rectangleObject in rectangleObjects)
                {
                    rectangleObject.draw(g, c, thickness);
                }
            }
            if (drawSquare == true)
            {
                foreach (Square squareObject in squareObjects)
                {
                    squareObject.draw(g, c, thickness);
                }
            }
            if (drawTriangle == true)
            {
                foreach (Triangle triangleObject in triangleObjects)
                {
                    triangleObject.draw(g, c, thickness);
                }
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Text Files|*.txt";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                System.IO.File.WriteAllText(sfd.FileName, richTextBox1.Text);
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Text Files|*.txt";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                richTextBox1.Text = System.IO.File.ReadAllText(ofd.FileName);
            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {

            MessageBox.Show("For Your Help:\n" +
                "draw circle 60\n" +
                "draw rectangle 100 50\n" +
                "draw triangle 10 100 100 10 200 200\n" +
                "draw square 50\n" +
                "move 100 100\n" +
                "color red 5\n" +
                "fill red\n" +
                "fill no\n");
        }

        private void button1_Click(object sender, EventArgs e)
        {

            char[] delimiters = new char[] { '\r', '\n' };
            string[] parts = richTextBox1.Text.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < parts.Length; i++)
            {
                CodeParse cm = new CodeParse();
                string b = cm.Parse(textBox1.Text, parts[i], i+1);
                
                char[] code_delimiters = new char[] { ' ' };
                string[] words = b.Split(code_delimiters, StringSplitOptions.RemoveEmptyEntries);
                switch (words[0])
                {
                    case "circle":
                        Circle circle = new Circle();
                        circle.setColor(fill);
                        circle.setX(moveX);
                        circle.setY(moveY);
                        circle.setRadius(Convert.ToInt32(words[1]));
                        circleObjects.Add(circle);
                        drawCircle = true;
                        break;
                    case "rectangle":
                        Rectangle rectangle = new Rectangle();
                        rectangle.setColor(fill);
                        rectangle.setX(moveX);
                        rectangle.setY(moveY);
                        rectangle.setHeight(Convert.ToInt32(words[1]));
                        rectangle.setWidth(Convert.ToInt32(words[2]));
                        rectangleObjects.Add(rectangle);
                        drawRectangle = true;
                        break;
                    case "square":
                        Square square = new Square();
                        square.setColor(fill);
                        square.setX(moveX);
                        square.setY(moveY);
                        square.setSize(Convert.ToInt32(words[1]));
                        squareObjects.Add(square);
                        drawSquare = true;
                        break;
                    case "triangle":
                        Triangle triangle = new Triangle();
                        triangle.setColor(fill);
                        triangle.setPoint(Convert.ToInt32(words[1]), Convert.ToInt32(words[2]), Convert.ToInt32(words[3]), Convert.ToInt32(words[4]), Convert.ToInt32(words[5]), Convert.ToInt32(words[6]));
                        triangleObjects.Add(triangle);
                        drawTriangle = true;
                        break;
                    case "clear":
                        circleObjects.Clear();
                        rectangleObjects.Clear();
                        squareObjects.Clear();
                        triangleObjects.Clear();
                        this.drawCircle = false;
                        this.drawTriangle = false;
                        this.drawRectangle = false;
                        this.drawSquare = false;
                        this.richTextBox1.Clear();
                        panel1.Refresh();
                        break;
                    case "move":
                        this.moveX = Convert.ToInt32(words[1]);
                        this.moveY = Convert.ToInt32(words[2]);
                        break;
                    case "reset":
                        this.moveX = 0;
                        this.moveY = 0;
                        fill = Color.Transparent;
                        c = Color.Black;
                        break;
                    case "color":
                        this.thickness = Convert.ToInt32(words[2]);
                        Color cs = Color.FromName(words[1]);

                        if (cs.IsKnownColor)
                        {
                            this.c = Color.FromName(char.ToUpper(words[1][0]) + words[1].Substring(1));
                        }
                        else
                        {
                            MessageBox.Show("please enter a valid color");
                        }
                        break;
                    case "fill":
                        Color fil = Color.FromName(words[1]);
                        if (fil.IsKnownColor)
                        {
                            this.fill = Color.FromName(char.ToUpper(words[1][0]) + words[1].Substring(1));
                        }
                        else if (words[1] == "no")
                        {
                            this.fill = Color.Transparent;
                        }
                        else
                        {
                            MessageBox.Show("please enter a valid color");
                        }
                        break;
                    default:
                        MessageBox.Show(b);
                        break;

                }
            }
            panel1.Refresh();
        }
    }
}
