﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Programming_application
{
    interface Shapes
    {
        void setX(int x);
        void setY(int y);
        void draw(Graphics g,Color c, int thickness);
        double calcArea();
        double calcPerimeter();
    }
}
