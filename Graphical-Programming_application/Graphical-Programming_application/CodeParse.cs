﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphical_Programming_application
{
    class CodeParse
    {
        public CodeParse() { }
        public string Parse(string comma, string code, int line)
        {
            int temp;
            Boolean error = false;
            string errorCmm;

            string comm = comma.ToLower();
            string cods = code.ToLower();

            switch (comm)
            {
                case "run":
                    try
                    {
                        String code_line = cods;
                        char[] code_delimiters = new char[] { ' ' };
                        String[] words = code_line.Split(code_delimiters, StringSplitOptions.RemoveEmptyEntries);
                        if (words[0] == "draw")
                        {
                            if (words[1] == "circle")
                            {
                                if (!(words.Length == 3))
                                {
                                    error = true;
                                    errorCmm = "the error in on line " + line + " please type correct code for 'draw circle'";
                                    return errorCmm;
                                }
                                else
                                {

                                    string shape = "circle " + words[2];
                                    return shape;
                                }
                            }
                            else if (words[1] == "square")
                            {
                                if (!(words.Length == 3))
                                {
                                    error = true;
                                    errorCmm = "the error in on line " + line + " pls type correct code for 'draw square'";
                                    return errorCmm;
                                }
                                else
                                {
                                    string shape = "square " + words[2];
                                    return shape;

                                }
                            }

                            else if (words[1] == "rectangle")
                            {
                                if (!(words.Length == 4))
                                {
                                    error = true;
                                    errorCmm = "the error in on line " + line + " pls type correct code for 'draw rectangle'";
                                    return errorCmm;
                                }
                                else
                                {
                                    string shape = "rectangle " + words[2] + " " + words[3];
                                    return shape;

                                }
                            }
                            else if (words[1] == "triangle")
                            {
                                if (!(words.Length == 8))
                                {
                                    error = true;
                                    errorCmm = "the error in on line " + line + " pls type correct code for 'draw triangle'";
                                    return errorCmm;
                                }
                                else
                                {
                                    string shape = "triangle " + words[2] + " " + words[3] + " " + words[4] + " " + words[5] + " " + words[6] + " " + words[7];
                                    return shape;
                                }
                                
                            }
                            else
                            {
                                return "false";
                            }
                        }
                        else if (words[0] == "move")
                        {
                            if (!(words.Length == 3))
                            {
                                error = true;
                                errorCmm = "the error in on line " + line + " pls type correct code for 'draw square'";
                                return errorCmm;
                            }
                            else
                            {
                                return ("move " + words[1] + " " + words[2]);
                            }
                        }
                        else if (words[0] == "color")
                        {
                            if (!(words.Length == 3))
                            {
                                error = true;
                                errorCmm = "the error in on line " + line + " pls type correct code for 'move'";
                                return errorCmm;
                            }
                            else if (Int32.TryParse(words[2], out temp))
                            {

                                return code_line;
                            }
                            else
                            {
                                return "pls enter the parameters in integers";

                            }
                        }
                        else if (words[0] == "fill")
                        {
                            if (!(words.Length == 2))
                            {
                                error = true;
                                errorCmm = "the error in on line " + line + " pls type correct code for 'move'";
                                return errorCmm;
                            }

                            else
                            {
                                return code_line;

                            }
                        }
                        else
                        {
                            return "false";

                        }


                    }
                    catch (IndexOutOfRangeException ex)
                    {
                        return "false";

                    }
                    catch (FormatException ex)
                    {
                        return "!!Please input correct parameter!!";
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        return "!!Please input correct parameter!!";
                    }
                    catch (NullReferenceException ex)
                    {
                        return "!!Please input correct parameter!!";
                    }
                    break;
                    
                case "clear":
                    return "clear";
                case "reset":
                    return "reset";
                default:
                    return "Please Enter Correct Command";

            }

        }
    }
}
